/*-------------------Modal Show-----------------------------*/

/*Var for Modal*/
var button = document.getElementsByClassName('white-block');
var modal_1 = document.getElementById('modal-1');
var modal_2 = document.getElementById('modal-2');

for ( i = 0; i < button.length; i++ ) {
	button[i].addEventListener("click", loadNews, false );
}

function loadNews(){
	var idOfElement;
	idOfElement = this.getAttribute('id');
	if(idOfElement == "news-1") {	
		modal_1.style.display = "block";
		document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
	}
	else if(idOfElement == "news-2") {	
		modal_2.style.display = "block";
		document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
	}
  	
}


/*---------------------------Modal close---------------*/

window.addEventListener('keydown', EscClose);

function EscClose(event) {
	
	if(event.keyCode === 27 ) {
		modal_1.style.display = "none";
		modal_2.style.display = "none";
		document.getElementsByTagName('html')[0].style.overflowY = 'scroll';											
}
};


/*-------Resize Window--------*/

window.addEventListener("resize", Resize);
												
 function Resize() {
	$('header').height(document.documentElement.clientHeight + 'px');
	$('div.main-slider-slide').height(document.documentElement.clientHeight + 'px'); 
};

$('div.main-slider-slide').height(document.documentElement.clientHeight + 'px'); 


/*-----------Google Map--------*/

document.body.onload = initialize;

var map = new google.maps.Map(document.getElementById("map-canvas"));

function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(50.469329, 30.601588),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);

    var markers = [],
        myPlaces = [];
    myPlaces.push(new Place('Оптика', 50.469329,30.601588, 30.625095, 'Киев, ул.Воскресінська, 16Г', '124124124'));

    var image = new google.maps.MarkerImage('images/1.png',      
      new google.maps.Size(150, 150),      
      new google.maps.Point(0,0),      
      new google.maps.Point(0, 32));

    for (var i = 0, n = myPlaces.length; i < n; i++) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(myPlaces[i].latitude, myPlaces[i].longitude),
            map: map,
            title: myPlaces[i].name
        });
        var infowindow = new google.maps.InfoWindow({
            content: '<h1>Магазин "Оптика"</h1><p class="map-adress"><i class="fa fa-map-marker" aria-hidden="true"></i>Киев, ул.Воскресінська, 16Г</p>' +
					'<p class="map-phone"><i class="fa fa-phone" aria-hidden="true"></i>+38 098 903 37 80</p>'
        });
        makeInfoWindowEvent(map, infowindow, marker);
        markers.push(marker);
    }
}
function makeInfoWindowEvent(map, infowindow, marker) {
        infowindow.open(map, marker);
    
}
function Place(name, latitude, longitude, description, phone){
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.description = description;
	  this.phone = phone
}

var d = document;
google.maps.event.addDomListener(window, 'load', initialize);

/*----------------------------------JQuery-----------------------------------------*/


$(document).ready(function() {
	
	/*Close modal thank early*/
	$("#modal-thank-close").on("click", function() {
		$("#modal-thank").fadeOut(400);
	});

	
 $('div.header-arrow').on('click', function(){
    $('html, body').animate({scrollTop: $(window).height()}, 500);
	 console.log($(window).height());
  });	
	
$('#header-line-button').on('click', function() {
	$('.header-line-menu').fadeIn();
    $(this).fadeOut();
});

$('#close-menu').on('click', function() {
	$('.header-line-menu').fadeOut();
    $('#header-line-button').fadeIn();
});

if ( $(window).width() < 768 ) {
  $('.header-line-menu > li > a').on('click', function() {
    $('.header-line-menu').fadeOut();
    $('#header-line-button').fadeIn();
  });
}
	
	/*var height = document.documentElement.clientHeight;
	
	$('header').height(height + 'px');*/

	
	$('.modal-window-close').on('click', function(e) {
		e.preventDefault();
		$('#modal-1, #modal-2').fadeOut();
		$('html').css('overflow-y','scroll');
	});
	
	/*-------------------------Slider Code--------------------------------------*/
	$('div.main-slider-slide').first().addClass('active-slide');
	
	var timer = 6000;
	var flag = true;
	
	var slideShowInterval = setInterval(nextSlide, timer);
	
	/*$('body').on('click', 'li.main-slider-dot', function() {
		if ( !$(this).hasClass('main-slider-dot-active') && flag === true ) {
			flag = false;
			$('li.main-slider-dot').removeClass('main-slider-dot-active');
			$(this).addClass('main-slider-dot-active');
			
			$('div.active-slide').removeClass('active-slide');
			$('div.main-slider-slide').eq($(this).index()).addClass('active-slide');
			
			clearInterval(slideShowInterval);
			slideShowInterval = setInterval(nextSlide, timer);
			
			setTimeout( function() { flag = true; }, 600);
		}
	});*/
	
	function nextSlide() {
		if ( flag === true ) {
			flag = false;
			
			if ( $('div.active-slide').next().length ) {
				$('div.active-slide').removeClass('active-slide').next().addClass('active-slide');
				/*$('li.main-slider-dot-active').removeClass('main-slider-dot-active').next().addClass('main-slider-dot-active');*/
			} else {
				$('div.active-slide').removeClass('active-slide');
				$('div.main-slider-slide').first().addClass('active-slide');
				/*$('li.main-slider-dot-active').removeClass('main-slider-dot-active');
				$('li.main-slider-dot').first().addClass('main-slider-dot-active');*/
			}
			
			clearInterval(slideShowInterval);
			slideShowInterval = setInterval(nextSlide, timer);
			
			setTimeout( function() { flag = true; }, 600);
		}
	}
	
	function prevSlide() {
		if ( flag === true ) {
			flag = false;
			
			if ( $('div.active-slide').prev().length ) {
				$('div.active-slide').removeClass('active-slide').prev().addClass('active-slide');
				/*$('li.main-slider-dot-active').removeClass('main-slider-dot-active').prev().addClass('main-slider-dot-active');*/
			} else {
				$('div.active-slide').removeClass('active-slide');
				$('div.main-slider-slide').last().addClass('active-slide');
				/*$('li.main-slider-dot-active').removeClass('main-slider-dot-active');
				$('li.main-slider-dot').last().addClass('main-slider-dot-active');*/
			}
			
			clearInterval(slideShowInterval);
			slideShowInterval = setInterval(nextSlide, timer);
			
			setTimeout( function() { flag = true; }, 600);
		}
	}
	
	var d = document, xDown, yDown, xMove, yMove, xDiff, yDiff, s;
	function handleTouchStart(e) {
		if ( e.type != 'touchstart') {
			xDown = e.pageX;
    	yDown = e.pageY;
		} else {
			xDown = e.touches[0].pageX;
    	yDown = e.touches[0].pageY;
		}
	}
	
	function handleTouchMove(e) {
		if ( e.type != 'touchmove') {
			xMove = e.pageX;
    	yMove = e.pageY;
		} else {
			xMove = e.touches[e.touches.length - 1].pageX;
    	yMove = e.touches[e.touches.length - 1].pageY;
		}
	}
	
	function handleTouchEnd(e) {
  	if (!xDown || !yDown || !xMove) {
  	  return;
  	}
		xDiff = xDown - xMove;
 	 	yDiff = yDown - yMove;
		
    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 20) {
				nextSlide();
      } else if (xDiff < -20) {
				prevSlide();
      }
		}
		xDown = yDown = xMove = yMove = null;
	}

	function handleMfp() {
		var mw = d.getElementById('main-slider');
		if (mw) {
			mw.addEventListener('touchstart', handleTouchStart, false);
			mw.addEventListener('touchmove', handleTouchMove, false);
			mw.addEventListener('touchend', handleTouchEnd, false);
		}
	}
	handleMfp();

	$(document).on('mousedown', '#main-slider', handleTouchStart);
	$(document).on('mousemove', '#main-slider', handleTouchMove);
	$(document).on('mouseup', '#main-slider', handleTouchEnd);
});

/*-----------CALENDAR scripts START----------------*/

/*---Init Variables---*/
var reserveTableDay = document.getElementsByClassName("reserve-table-day")[0];

var calendarLabelMonthYear = document.getElementsByClassName("header-calendar-current-month")[0]; 

var displayDate = new Date();

var currentDate = new Date();

var selectedDate = new Date();

var headerCalendar = document.getElementsByClassName("header-calendar")[0];
var calendarTableData = headerCalendar.getElementsByTagName("TD");

var selectedIndexTD;

document.getElementsByClassName("header-calendar-right")[0].addEventListener('click', CalendarRightArrowClick);

/*---Init Listeners---*/

var headerCalendarLeft = document.getElementsByClassName("header-calendar-left")[0];
headerCalendarLeft.addEventListener('click', CalendarLeftArrowClick);

reserveTableDay.addEventListener('click', ShowCalendar);

/*MAy delete later*/
/*var wrapperForCalendar = document.getElementsByClassName('wrapper-for-calendar')[0];
wrapperForCalendar.addEventListener('click', closeCalendarBackClick);*/

function closeCalendarBackClick (e) {
	//  wrapperForCalendar.style.display = 'none';
	headerCalendar.style.display = 'none';	
}

SetListenersForTD();
inititialization();

function inititialization() {
	var monthDisplay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	
	var firstDayOfMonth = (monthDisplay.getDay() == 0) ? 6 : (monthDisplay.getDay() - 1);
	selectedIndexTD = firstDayOfMonth + currentDate.getDate() - 1;
	
	console.log(firstDayOfMonth);
	console.log(currentDate.getDate());
	setIndexTD();
}

function setIndexTD(){
	for(var i = 0; i < calendarTableData.length; i++) {
		calendarTableData[i].tdIndex = i;
	}
}

function SetListenersForTD () {
	
	for(var i = 0; i < 42; i++) {
		calendarTableData[i].addEventListener('click', ClickOnCalendarTableData);
	}
}

/*-------Init reserve-table-day--------*/
SetReserveTableDay();

/*---Method Itself---*/

/*Chose Day and month, and after close*/
function ClickOnCalendarTableData (e) {
	
	if(this.className == "not-in-month" || this.className == "disabled" ||
	  this.className == "availeble selected" || this.className == "current selected") {
		return;
	}
	
	else if(this.className == "current") {
		ClearSelected();
		setSelectedDate(this.innerHTML);
		SetReserveTableDay();
	}
	
	else {
		ClearSelected();
		this.className += " selected";
		selectedIndexTD = this.tdIndex;
		setSelectedDate(this.innerHTML);
		SetReserveTableDay();
	}
	
	headerCalendar.style.display = "none";
	
}

function setSelectedDate(Date) {
	
	displayDate.setDate(Date);
	selectedDate.setDate(Date);
	selectedDate.setMonth(displayDate.getMonth());
	selectedDate.setFullYear(displayDate.getFullYear());
	
}

/*just clear selected date in calendar*/
function ClearSelected () {
	for (var i =0; i < 42; i++) {
			calendarTableData[i].className = calendarTableData[i].className.replace(" selected" , '');
		}
}

/*Set Reserve table data*/
function SetReserveTableDay () {
	reserveTableDay.innerHTML =  "<span>" +  displayDate.getDate() +'/' + (displayDate.getMonth() + 1) + "/" + displayDate.getFullYear() + "</span>";
}

function SetCalendar(month, year, thisMonth) {
	
	var monthDisplay = new Date(year, month, 1);
	
	var firstDayOfMonth = (monthDisplay.getDay() == 0) ? 6 : (monthDisplay.getDay() - 1);
	
	var dayInMonth = getDaysInMonth(month, year);
	var dayInPrevMonth = month==0 ? getDaysInMonth(11, year-1) : getDaysInMonth(month - 1, year);

	/*Set days of previous month*/
	for(var i = firstDayOfMonth-1; i >= 0; i--, dayInPrevMonth-- ) {
		
		calendarTableData[i].innerHTML = dayInPrevMonth;
		calendarTableData[i].className = "not-in-month";
	}
	
	/*Set days in current month*/
	if (thisMonth) {
		for(var i = 0; i < dayInMonth + 1; i++) {
			calendarTableData[i + firstDayOfMonth].innerHTML = i+1;
			if(currentDate.getDate() - 1 > i) {
                console.log(i + firstDayOfMonth);
				calendarTableData[i + firstDayOfMonth].className = "disabled";
				
				console.log("if " + (i + firstDayOfMonth));
			}
		
			else if(currentDate.getDate() == i ) {
				calendarTableData[i + firstDayOfMonth - 1 ].className = "current";
				
				console.log("else if " + (i + firstDayOfMonth));
				console.log(calendarTableData[i + firstDayOfMonth - 1].className );
			}
		
			else if(currentDate.getDate() < i ) {
				calendarTableData[i + firstDayOfMonth  -1].className = "availeble";
				
				//console.log("else " + (i + firstDayOfMonth -1 ));
			}
	}
		console.log(currentDate.getDate());
	}
	else {
		for(var i =0; i < dayInMonth ; i++) {
			calendarTableData[i + firstDayOfMonth].innerHTML = i+1;
			calendarTableData[i + firstDayOfMonth].className = "+ availeble";
		}
	}
	
	/*Set days in next month */		
	for( var i = 1, index = dayInMonth + firstDayOfMonth; index < 42; i++, index++) {
			calendarTableData[index].innerHTML = i;
			calendarTableData[index].className = "not-in-month";
	}
	
	if(displayDate.getMonth() == selectedDate.getMonth() && displayDate.getFullYear() == selectedDate.getFullYear()) {
		calendarTableData[selectedIndexTD].className += " selected";
	}
	
}


/*Right arrow click and show next month dates*/
function CalendarRightArrowClick (event) {
	
	event.preventDefault();
	
	headerCalendarLeft.style.display = "block";
	
	displayDate = new Date(displayDate.getFullYear(), displayDate.getMonth() + 1 , 1);
	SetCalendar(displayDate.getMonth() , displayDate.getFullYear(), false);
	calendarLabelMonthYear.innerHTML = getMonthName(displayDate.getMonth()) + " " + displayDate.getFullYear();
}

/*Left arrow click and show the prev month if it current month disable left arrow*/
function CalendarLeftArrowClick(event) {
	
	event.preventDefault();
	
	displayDate = new Date(displayDate.getFullYear(), displayDate.getMonth() - 1 , 1);
	
	if(displayDate.getMonth() == currentDate.getMonth() && displayDate.getFullYear() == currentDate.getFullYear()) {
		SetCalendar(displayDate.getMonth() , displayDate.getFullYear(), true);
		this.style.display = "none";
	}
	
	else {
		SetCalendar(displayDate.getMonth() , displayDate.getFullYear(), false);
	}
	
	calendarLabelMonthYear.innerHTML = getMonthName(displayDate.getMonth()) + " " + displayDate.getFullYear();
}

function ShowCalendar(e) {
	
	if(displayDate.getMonth() == currentDate.getMonth() && displayDate.getFullYear() == currentDate.getFullYear()) {
		SetCalendar(displayDate.getMonth() , displayDate.getFullYear(), true);
	} else {
		SetCalendar(displayDate.getMonth() , displayDate.getFullYear(), false);
	}
	
	
	if(headerCalendar.style.display == "block")
		{
			headerCalendar.style.display = "none";
			return;
		}
	
	headerCalendar.style.display = "block";
	
	console.log(getMonthName(displayDate.getMonth()) + " " + displayDate.getFullYear());
	calendarLabelMonthYear.innerHTML = getMonthName(displayDate.getMonth()) + " " + displayDate.getFullYear();
	
}

function getMonthName(monthNumber) {
		
	switch(monthNumber) {
		case 0: return "Январь";
		case 1: return "Февраль";
		case 2: return "Март";
		case 3: return "Апрель";
		case 4: return "Май";
		case 5: return "Июнь";
		case 6: return "Июль";
		case 7: return "Август";
		case 8: return "Сентябрь";
		case 9: return "Октябрь";
		case 10: return "Ноябрь";
		case 11: return "Декабрь";
	}
}
	
function getDaysInMonth(month, year) {
     var date = new Date(year, month, 1);
     var days = 0;
     while (date.getMonth() === month) {
        days++;
        date.setDate(date.getDate() + 1);
     }
     return days;
}

/*-----------CALENDAR scripts END----------------*/

/*-----------Send request block START------------*/

var sendBtnRequest = d.getElementById("send-request");

sendBtnRequest.addEventListener("click", SendRequest);

function SendRequest() {

    if(!validationSubmitForm()) return;
    console.log("Validation Success");
	
    var xhr = new XMLHttpRequest();
    var bodyRequest = formReview();

    xhr.open('POST', '/content' + '?' + bodyRequest, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    console.log(window.location.href);
	console.log(document.URL);
    xhr.onreadystatechange = function() {
        if (this.readyState != 4) return;
        if (xhr.status != 200) {
            console.log("Error send Review");
        } else {
            console.log(this.responseText);
        }
    }
    console.log(bodyRequest);
    //xhr.send(bodyRequest);
	xhr.send();

	ShowModalThank();
	clearFields();
}

function validationSubmitForm() {
    var name = d.getElementById("name");
    var email = d.getElementById("email");
    var phone = d.getElementById("phone");
    /*var time = d.getElementById("time");*/
    var comment = d.getElementById("comment");
    var returnVal = true;
    
    /*NAME CHECK*/
    if(name.value.match(/^[\s]{0,2}[а-яА-ЯёЁa-zA-ZіїєґІЇЄҐ]+[\s]{1}[а-яА-ЯёЁa-zA-ZіїєґІЇЄҐ]+[\s]?([\s]{1}[а-яА-ЯёЁa-zA-ZіїєґІЇЄҐ]+)?$/) &&
      name.value.length > 5) {
        removeClass(name, "wrong");
        addClass(name, "correct");
        
    } else {
        returnVal = false;
        addClass(name, "wrong");
        removeClass(name, "correct");
    }
    /*EMAIL CKECK*/
    if(email.value.match(/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+$/)) {
        removeClass(email, "wrong");
        addClass(email, "correct");
    } else {
        
        returnVal = false;
        addClass(email, "wrong");
        removeClass(email, "correct");
    }
    /*PHONE CHECK*/
    if(phone.value.match(/^[+]?[0-9\-\s]+$/) && (phone.value.length > 6) && (phone.value.length < 20)) {
       
        removeClass(phone, "wrong");
        addClass(phone, "correct");
    } else {
        returnVal = false;
        addClass(phone, "wrong"); 
        removeClass(phone, "correct")
    } 
    
    /*Comment CHECK*/
    if(comment.value.length > 10) {
        removeClass(comment, "wrong");
        addClass(comment, "correct");
    } else {
        returnVal = false;
        addClass(comment, "wrong"); 
        removeClass(comment, "correct")
    }
    return returnVal;
    
}

function formReview() {
    var name = d.getElementById("name").value;
    var phone = d.getElementById("phone").value;
    var email = d.getElementById("email").value;
    /*var time = d.getElementById("time");*/
    var comment = d.getElementById("comment").value;
    var date = d.getElementById("span-date").firstChild.innerHTML;
    /*time = time.options[time.selectedIndex].value;*/
    
    return "name=" + name + "&phone=" + phone + "&email=" + email + /*"&time=" + time +*/ "&comment=" +
        comment + "&date=" + date;
}

function clearFields() {
	var name = d.getElementById("name");
    var email = d.getElementById("email");
    var phone = d.getElementById("phone");
    /*var time = d.getElementById("time");*/
    var comment = d.getElementById("comment");
	
	name.value = "";
	email.value = "";
	phone.value = "";
	comment.value = "";
	
	removeClass(name, "correct");
	removeClass(email, "correct");
	removeClass(phone, "correct");
	removeClass(comment, "correct");

}

function ShowModalThank (e) {
	
	$("#modal-thank").fadeIn(300);
	setTimeout(function() {
		$("#modal-thank").fadeOut(400);
	}, 5500);
	
}

/*-----------Send request block END--------------*/

/*====================================GLOBAL functions*==================================================*/
function removeClass(obj, cls) {   
    
    var classes = obj.className.split(" ");
    
    for (i = 0; i < classes.length; i++) {    
      if(classes[i] == cls) {      
          classes.splice(i, 1); // удалить класс     
          i -= 1;     
      }   
  } 
    obj.className = classes.join(" ");
}


function addClass(obj, cls) {   
    
var classes = obj.className ? obj.className.split(" ") : [];
   
  for (var i = 0; i < classes.length; i++) {    
     
      if (classes[i] == cls) return; // класс уже есть 
  }
  classes.push(cls); // добавить
  obj.className = classes.join(" "); // и обновить свойство 
}
    
function hasClass(obj, cls) {
    
    if(obj.className.indexOf(cls) > -1 ) {
        return true;
    } else {
        return false;
    }
}   
