from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models import CMSPlugin
from django.utils.translation import ugettext as _
from .models import *

class TextWithoutTagPlugin(CMSPluginBase):
    model = simpleTextModel
    module = _("opticasite")
    name = _('Simple Text')
    render_template = 'SimpleText.html'
    def render(self, context, instance, placeholder):
        context = super(TextWithoutTagPlugin, self).render(context, instance, placeholder)
        return context

plugin_pool.register_plugin(TextWithoutTagPlugin)