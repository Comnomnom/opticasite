from django.db import models
from django.utils.translation import ugettext as _
from cms.models import CMSPlugin

class simpleTextModel(CMSPlugin):
    text = models.CharField(max_length=1000, default='', verbose_name=_("Text"))

    def __unicode__(self):
        return self.name