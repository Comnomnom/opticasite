# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='simpleTextModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, related_name='custom_optica_plugins_simpletextmodel', primary_key=True, serialize=False, to='cms.CMSPlugin', parent_link=True)),
                ('text', models.CharField(max_length=1000, verbose_name='Текст', default='')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
